use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use piston::input::keyboard::Key;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };

pub struct App {
    gl: GlGraphics, // OpenGL drawing backend.
    rotation: f64,  // Rotation for the square.
    x: f64,         // Position
    y: f64,         // of the box
    keys: i64,      // Keys
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
        const RED:   [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        let square = rectangle::square(0.0, 0.0, 50.0);
        let rotation = self.rotation;

        let (x, y) = (self.x, self.y);

        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(GREEN, gl);

            let transform = c.transform.trans(x, y)
                                       .rot_rad(rotation)
                                       .trans(-25.0, -25.0);

            // Draw a box rotating around the middle of the screen.
            rectangle(RED, square, transform, gl);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        // Rotate 2 radians per second.
        self.rotation += 2.0 * args.dt;
        let (up, down, left, right) = (
            self.keys & 0x1 != 0,
            self.keys & 0x2 != 0,
            self.keys & 0x4 != 0,
            self.keys & 0x8 != 0,
        );
        if up    { self.y -= 200.0 * args.dt; }
        if down  { self.y += 200.0 * args.dt; }
        if left  { self.x -= 200.0 * args.dt; }
        if right { self.x += 200.0 * args.dt; }
    }

    fn button(&mut self, args: &ButtonArgs) {
        use self::ButtonState::*;
        use self::Button::*;
        match (args.state, args.button) {
            (Press, Keyboard(Key::Up)) => self.keys |= 0x1,
            (Press, Keyboard(Key::Down)) => self.keys |= 0x2,
            (Press, Keyboard(Key::Left)) => self.keys |= 0x4,
            (Press, Keyboard(Key::Right)) => self.keys |= 0x8,
            (Release, Keyboard(Key::Up)) => self.keys &= !0x1,
            (Release, Keyboard(Key::Down)) => self.keys &= !0x2,
            (Release, Keyboard(Key::Left)) => self.keys &= !0x4,
            (Release, Keyboard(Key::Right)) => self.keys &= !0x8,
            _ => (),
        }
    }
}

fn main() {
    // Change this to OpenGL::V2_1 if not working.
    let opengl = OpenGL::V3_2;

    // Create an Glutin window.
    let mut window: Window = WindowSettings::new(
            "xil e kleagalis",
            [1280, 960]
        )
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    // Create a new game and run it.
    let mut app = App {
        gl: GlGraphics::new(opengl),
        rotation: 0.0,
        x: 640.0,
        y: 480.0,
        keys: 0,
    };

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            app.render(&r);
        }

        if let Some(u) = e.update_args() {
            app.update(&u);
        }

        if let Some(b) = e.button_args() {
            app.button(&b);
        }
    }
}
