use fixed::frac;
use fixed::FixedI32;
use nalgebra_glm::TVec2;

pub type Dist = FixedI32<frac::U16>;
pub type Pos = TVec2<Dist>;
pub type Sc = FixedI32<frac::U30>;
